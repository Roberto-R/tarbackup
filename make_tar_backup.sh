#!/bin/bash

d=$(date +%Y-%m-%d_%H%M%S)

# log_file="/var/log/tar_backup.log"

filename="/data/backups/backup_$d.tar.gz"

echo ""
echo "Tar backup started --- $d"
echo "Destination: $filename"
echo "Backing up as: $(whoami)"

# - c Create
# - p Keep permission and owners
# - z Compressed
# - f Put result in file
# Make backup of '/', by changing dir first

cd /

tar -cpzf $filename \
--exclude=data \
--exclude=usr \
--exclude=snap \
--exclude=dev \
--exclude=proc \
--exclude=run \
--exclude=sys \
--exclude=tmp \
--exclude=media \
--exclude=lost+found \
--exclude=var/cache/samba \
--exclude=var/lib/samba/private \
./

# Check tar result
if [ $? == 0 ]; then
        echo "Backup successful!"
else
        echo "Something went wrong..."
fi

python3 /data/backups/backup_cleaner.py

d=$(date +%Y-%m-%d_%T)

echo "Stopped at $d"

# Add empty line to log
echo ""